let addUserFormEl = document.getElementById("addUserForm")
let AllUsersData = []
let userData = {
    name:"",
    status: "",
    gender: "",
    married: false
}


let userInputEl = document.getElementById("userInput")
let workingStatusEl = document.getElementById("workingStatus")

let maleEl = document.getElementById("male")
let femaleEl = document.getElementById("female")

let checkboxEl = document.getElementById("checkbox")

let errorEl = document.querySelector(".error")

userInputEl.addEventListener("change", (event) =>{
    userData.name = event.target.value
})

workingStatusEl.addEventListener("change", (event) =>{
    userData.status = event.target.value
})

maleEl.addEventListener("change",(event) =>{
    userData.gender = event.target.value
})

femaleEl.addEventListener("change", (event) =>{
    userData.gender = event.target.value
})

checkboxEl.addEventListener("change", (event) =>{
    if(event.target.checked){
        userData.married = true
    }
})

addUserFormEl.addEventListener("submit", (event)=>{
    event.preventDefault()
    AllUsersData.push(userData)
    if (userInputEl.value === ""){
         errorEl.textContent = "*Require"
    }else{
        errorEl.textContent = ""
    }
})



