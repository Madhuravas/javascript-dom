let boxes = document.getElementsByClassName("box")
let box2 = document.getElementById("helo")

const counterHover = () =>{
    let count = 0 
    let onHover = ()=>{
          count++
    }
    return {
        onHover,
        getcount: () => count
    }
}

if(boxes){
    for(let i=0; i<boxes.length; i++){
       const myCounter = counterHover()
       boxes[i].addEventListener("mouseover",(event) =>{
        myCounter.onHover()
        event.target.innerText = myCounter.getcount()
       })
    }
}



